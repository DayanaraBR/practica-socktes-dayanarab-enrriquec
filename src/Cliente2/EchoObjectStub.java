package Cliente2;

import client.*;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import rmi.EchoInt;

public class EchoObjectStub implements EchoInt {

  private Socket echoSocket = null;
  private PrintWriter os = null;
  private BufferedReader is = null;
  private String host = "localhost";
  private int port=7;
  private String output = "Error";

  public EchoObjectStub() {
	  }
  
  public EchoObjectStub(String host, int port) {
    this.host= host; this.port =port;
  }

  public String echo(String input)
  {
    connect();
    if (echoSocket != null && os != null && is != null) {
  	try {
             os.println(input);
             os.flush();
             output= is.readLine();
      } catch (IOException e) {
        System.err.println("I/O failed in reading/writing socket");
      }
    }
    disconnect();
    return output;
  }

  private synchronized void connect()
  {
	//EJERCICIO: Implemente el m�todo connect
      
	  try {
          echoSocket = new Socket(host, port);
          is = new BufferedReader( new InputStreamReader(echoSocket.getInputStream()));
          os = new PrintWriter(echoSocket.getOutputStream());
            } catch (Exception e) {
          System.out.println("Error de conexion: "+e.getMessage());
          echoSocket = null;   
          is = null;
          os = null; 
            }	
	    
  }

  private synchronized void disconnect(){ 
	//EJERCICIO: Implemente el m�todo disconnect
        
        try{
         echoSocket.close();
      }catch(Exception e){
          
      }
      echoSocket = null;   
      is = null;
      os = null;              
  }
	  
	  
	  
	  
	  
  
}
